
var Canvas;//canvas
var ctx;//use this to 


//initial
(function SettingCanvas(callback){
    $("body").hide();
    $("#myColor").hide();
    $("#uploadFile").hide();
    Canvas = document.getElementById("myCanvas");
    if(Canvas.getContext){
        ctx = Canvas.getContext("2d");
        ctx.lineCap = 'round';
        callback();
    }
})(StartToDraw);


function StartToDraw(){
    $("body").show(500);
}

//To implement the snap of canvas
var DataArray = new Array();//array to store the snap
var index = -1;//the index of the array
function Push(){
    index++;
    if(index<DataArray.length)//push the data at the correct position to delete the old data
        DataArray.length = index;
    DataArray.push(Canvas.toDataURL());
}
function Undo(){
    
    if(index==0){//canvas contains nothing
        index--;
        ctx.clearRect(0, 0, Canvas.width, Canvas.height);
    }
    else if(index>0){
        index--;
        var snap = new Image();
        snap.src = DataArray[index];
        ctx.globalCompositeOperation = "copy";//use this attribute to ignore the destination img
        snap.onload = function(){
            ctx.drawImage(snap, 0, 0);
            ctx.globalCompositeOperation = "source-over";//return to default
        }   
    }
}
function Redo(){
    if(index<DataArray.length-1){
        index++;
        var snap = new Image();
        snap.src = DataArray[index];
        ctx.globalCompositeOperation = "copy";
        snap.onload = function(){
            ctx.drawImage(snap, 0, 0);
            ctx.globalCompositeOperation = "source-over";
        }
    }
}

//return the mouse position
function mousePosition(e){
    var mousePos = {//mouse position
        x: e.clientX - Canvas.getBoundingClientRect().left,
        y: e.clientY - Canvas.getBoundingClientRect().top
    };
    return mousePos;
}

$(document).ready(function(){
    $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
        var pos = mousePosition(e);
        document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
    });

    $("#pen").click(function(){
        $("#myCanvas").css({"cursor": "url('pen_cursor_small.png'), auto"});//change the cursor
        $("#myCanvas").off('mousedown');//remove the bind between mousedown and canvas
        $("#myCanvas").on('mousedown', function (e){//create new own bind between mousedown versus canvas and implement the function
            ctx.strokeStyle = document.getElementById("myColor").value;//get the color
            ctx.lineWidth = document.getElementById("penThickness").value;//get the penthickness
            ctx.lineJoin = 'round';
            var mousePos = mousePosition(e);
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y+35);//modify 35 for y-position
            $("#myCanvas").on('mousemove', mousemoveHandler);//add the mousemove
            $("#myCanvas").off('mouseup');//remove the bind between mouseup and canvas
            $("#myCanvas").on('mouseup', function(){//create a bind to remove mousemove
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                Push();
            });
            function mousemoveHandler(e){
                mousePos = mousePosition(e);
                ctx.lineTo(mousePos.x, (mousePos.y+35));//the 35 for the image which doesn't point to left top.
                ///ctx.closePath();//a funny way
                ctx.stroke();//ctx.fill();
            }
        });
        //Canvas.addEventListener('mousedown', mousedownHandler);//This function is the same as the above function.
        //But this is hard to remove the eventlistener outside this block
    });
    var fillorstroke = true;
    $("#FillorStroke").click(function(){
        if(fillorstroke){
            $("#FillorStroke").animate({width: '+=50'}, "slow");
            $("#FillorStroke_img").animate({opacity: '0.5'}, "slow");
        }
        else{
            $("#FillorStroke").animate({width: '-=50'}, "slow");
            $("#FillorStroke_img").animate({opacity: '1'}, "slow");
        }
        fillorstroke = !fillorstroke;
    });
    var regular = true;
    $("#Regular").click(function(){
        if(regular){
            $("#Regular").animate({width: '+=50'}, "slow");
            $("#Regular_img").animate({opacity: '0.5'}, "slow");
        }
        else{
            $("#Regular").animate({width: '-=50'}, "slow");
            $("#Regular_img").animate({opacity: '1'}, "slow");
        }
        regular = !regular;
    });
    $("#rectangle").click(function(){
        $("#myCanvas").css("cursor", "crosshair");
        $("#myCanvas").off('mousedown');
        $("#myCanvas").on('mousedown', function(e){
            
            ctx.strokeStyle = document.getElementById("myColor").value;
            ctx.fillStyle = document.getElementById("myColor").value;
            ctx.lineWidth = document.getElementById("penThickness").value;
            var mousePosS = mousePosition(e);//Start Position
            var mousePosE = Object.create(mousePosS);//End Position

            $("#myCanvas").on('mousemove', mousemoveHandler);
            $("#myCanvas").off('mouseup');
            $("#myCanvas").on('mouseup', function(e){
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                Push();//this motion is done, so we push 
            });
            var temp = new Image();
            temp.src = Canvas.toDataURL();
            function mousemoveHandler(e){
                ctx.globalCompositeOperation = "copy";
                ctx.drawImage(temp, 0, 0);
                ctx.globalCompositeOperation = "source-over"; 
                mousePosE = mousePosition(e);
                if(fillorstroke){
                    if(regular){
                        let min = Math.min((mousePosE.x-mousePosS.x), (mousePosE.y-mousePosS.y));
                        ctx.fillRect(mousePosS.x, mousePosS.y, min, min);
                    }else{
                        ctx.fillRect(mousePosS.x, mousePosS.y, (mousePosE.x-mousePosS.x), (mousePosE.y-mousePosS.y));
                    }        
                }else{
                    if(regular){
                        let min = Math.min((mousePosE.x-mousePosS.x), (mousePosE.y-mousePosS.y));
                        ctx.strokeRect(mousePosS.x, mousePosS.y, min, min);
                    }else{
                        ctx.strokeRect(mousePosS.x, mousePosS.y, (mousePosE.x-mousePosS.x), (mousePosE.y-mousePosS.y));
                    }
                } 
            }
        });
    });
    $("#circle").click(function(){
        $("#myCanvas").css("cursor", "crosshair");
        $("#myCanvas").off('mousedown');
        $("#myCanvas").on('mousedown', function(e){
            ctx.strokeStyle = document.getElementById("myColor").value;
            ctx.fillStyle = document.getElementById("myColor").value;
            ctx.lineWidth = document.getElementById("penThickness").value;
            var mousePosS = mousePosition(e);
            var mousePosE = Object.create(mousePosS);
            
            $("#myCanvas").on('mousemove', mousemoveHandler);
            $("#myCanvas").off('mouseup');
            $("#myCanvas").on('mouseup', function(e){
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                Push();
            });
            var temp = new Image();
            temp.src = Canvas.toDataURL();
            function mousemoveHandler(e){
                ctx.globalCompositeOperation = "copy";
                ctx.drawImage(temp, 0, 0);
                ctx.globalCompositeOperation = "source-over"; 
                ctx.beginPath();
                mousePosE = mousePosition(e);
                if(regular){
                    let min = Math.min(Math.abs(mousePosE.y-mousePosS.y), Math.abs(mousePosE.x-mousePosS.x));
                    ctx.ellipse(mousePosS.x, mousePosS.y, min, min, Math.PI/2, 0, 2*Math.PI, true);
                }else{
                    ctx.ellipse(mousePosS.x, mousePosS.y, Math.abs(mousePosE.y-mousePosS.y), Math.abs(mousePosE.x-mousePosS.x), Math.PI/2, 0, 2*Math.PI, true);
                }
                
                if(fillorstroke)
                    ctx.fill();
                else
                    ctx.stroke();     
            }
        });
    });
    $("#triangle").click(function(){
        $("#myCanvas").css("cursor", "crosshair");
        $("#myCanvas").off('mousedown');
        $("#myCanvas").on('mousedown', function(e){
            ctx.strokeStyle = document.getElementById("myColor").value;
            ctx.fillStyle = document.getElementById("myColor").value;
            ctx.lineWidth = document.getElementById("penThickness").value;
            var mousePosS = mousePosition(e);
            var mousePosE = Object.create(mousePosS);

            $("#myCanvas").on('mousemove', mousemoveHandler);
            $("#myCanvas").off('mouseup');
            $("#myCanvas").on('mouseup', function(e){
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                Push();
            });
            var temp = new Image();
            temp.src = Canvas.toDataURL();
            function mousemoveHandler(e){
                ctx.globalCompositeOperation = "copy";
                ctx.drawImage(temp, 0, 0);
                ctx.globalCompositeOperation = "source-over";
                ctx.beginPath();
                mousePosE = mousePosition(e);
                if(regular){
                    
                    var diffx = mousePosE.x-mousePosS.x;
                    var diffy = mousePosE.y-mousePosS.y;
                    if(Math.abs(diffy) > Math.abs(diffx)){
                        if(diffy>=0){
                            mousePosE.y = mousePosS.y+(Math.abs(diffx)*Math.sqrt(3)/2);
                        }else{
                            mousePosE.y = mousePosS.y-(Math.abs(diffx)*Math.sqrt(3)/2);
                        }
                    }else{
                        if(diffx>=0){
                            mousePosE.x = mousePosS.x+(Math.abs(diffy)*2/Math.sqrt(3));
                        }else{
                            mousePosE.x = mousePosS.x-(Math.abs(diffy)*2/Math.sqrt(3));
                        }
                    }
                }
                ctx.moveTo((mousePosS.x+mousePosE.x)/2, mousePosS.y);
                ctx.lineTo(mousePosE.x, mousePosE.y);
                ctx.lineTo(mousePosS.x, mousePosE.y);
                ctx.closePath();
                
                
                if(fillorstroke)
                    ctx.fill();
                else
                    ctx.stroke();    
            }
        });
    });
    $("#line").click(function(){
        $("#myCanvas").css("cursor", "crosshair");
        $("#myCanvas").off('mousedown');
        $("#myCanvas").on('mousedown', function(e){
            ctx.strokeStyle = document.getElementById("myColor").value;
            ctx.fillStyle = document.getElementById("myColor").value;
            ctx.lineWidth = document.getElementById("penThickness").value;
            var mousePosS = mousePosition(e);
            var mousePosE = Object.create(mousePosS);

            $("#myCanvas").on('mousemove', mousemoveHandler);
            $("#myCanvas").off('mouseup');
            $("#myCanvas").on('mouseup', function(e){
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                Push();
            });
            var temp = new Image();
            temp.src = Canvas.toDataURL();
            function mousemoveHandler(e){
                ctx.globalCompositeOperation = "copy";
                ctx.drawImage(temp, 0, 0);
                ctx.globalCompositeOperation = "source-over"; 
                ctx.beginPath();
                mousePosE = mousePosition(e);
                ctx.moveTo(mousePosS.x, mousePosS.y);
                ctx.lineTo(mousePosE.x, mousePosE.y);
                ctx.stroke();    
            }
        });
    });
    
    
    $("#redo").click(function(){
        Redo();
    });
    $("#undo").click(function(){
        Undo();
    });

    $("#upload").click(function(){
        $("#uploadFile").off('change');
        $("#uploadFile").on('change',function(e){
            var reader = new FileReader();

            reader.onload = function(event){//after onload
                var img = new Image();
                img.onload = function(){
                    ctx.drawImage(img,0,0);
                    Push();
                }
                img.src = event.target.result;
            }
            reader.readAsDataURL(e.target.files[0]); 
        });
        
    });
    $("#btndownload").click(function(){
        var image = Canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");//change the Mime-type
        document.getElementById("download").setAttribute("href", image);//set the download resource
    });

    $("#text").click(function(){
        $("#myCanvas").css("cursor", "text");
        

        $("#myCanvas").off('mousedonw');
        $("#myCanvas").on('mousedown', function(e){
            
            var mousePosS = mousePosition(e);
            var mousePosE = Object.create(mousePosS);//End Position
            
            $("#myCanvas").on('mousemove', mousemoveHandler);
            $("#myCanvas").off('mouseup');
            $("#myCanvas").on('mouseup', function(e){
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                $("#myCanvas").off('mousedown');
                ctx.globalCompositeOperation = "copy";
                ctx.drawImage(temp, 0, 0);
                ctx.globalCompositeOperation = "source-over";
                
                var textspace = $('<textarea>',{id:'myTextarea'}).css({
                    'position':'absolute',
                    'width':Math.abs(mousePosE.x-mousePosS.x),
                    'height':Math.abs(mousePosE.y-mousePosS.y),
                    'left': mousePosS.x+20,
                    'top': mousePosS.y+20,
                });
                $('.content.canvas').append(textspace);
                
                
                $("#myCanvas").on('mousedown', function(e){
                    var font_attr = document.getElementById("font_size").value + "px " + document.getElementById("font_family").value;
                    ctx.font = font_attr;
                    ctx.globalCompositeOperation = "source-over";
                    ctx.strokeStyle = document.getElementById("myColor").value;
                    ctx.fillText(document.getElementById("myTextarea").value, mousePosS.x+20, mousePosS.y+20);  
                    $("#myTextarea").remove();
                    Push();
                    $("#myCanvas").off('mousedown');
                    $("#myCanvas").off('mouseup');
                });
                
            });
            var temp = new Image();
            temp.src = Canvas.toDataURL();
            function mousemoveHandler(e){
                ctx.globalCompositeOperation = "copy";
                ctx.drawImage(temp, 0, 0);
                ctx.globalCompositeOperation = "source-over"; 
                mousePosE = mousePosition(e);
                ctx.strokeRect(mousePosS.x, mousePosS.y, (mousePosE.x-mousePosS.x), (mousePosE.y-mousePosS.y));     
            }
        });
    });
    $("#eraser").click(function(){ 
        $("#myCanvas").css({"cursor": "url('eraser_cursor_small.png'), auto"});
        $("#myCanvas").off('mousedown');
        $("#myCanvas").on('mousedown', function (e){
            var mousePos = mousePosition(e);
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            $("#myCanvas").on('mousemove', mousemoveHandler);
            $("#myCanvas").off('mouseup');
            $("#myCanvas").on('mouseup', function(){
                $("#myCanvas").off('mousemove');
                $("#myCanvas").on('mousemove', function(e){//let the footer show the coordinate
                    var pos = mousePosition(e);
                    document.getElementById('mouseposition').innerHTML = "x: "+pos.x+"  y: "+pos.y;
                });
                Push();
            });
            function mousemoveHandler(e){
                mousePos = mousePosition(e);
                ctx.clearRect(mousePos.x-10, mousePos.y, 50, 50);//the number here is for modify the work space
            }
        });
    });
    $("#clear").click(function(){
        $("#myCanvas").css("cursor", "default");
        $("#myCanvas").off('mousedown');
        ctx.clearRect(0, 0, Canvas.width, Canvas.height);
    });

});
