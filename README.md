# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

首先，從HTML檔開始，先將整個分成4塊:上面標題、左邊工具欄、右邊畫布、以及下方footer
上方標題主要就是透過一些css來修飾好看一些
而下方footer則是會顯示你的滑鼠在畫布中的位置(x,y)
主要的實現功能是在左方工具欄跟右方畫布中完成：

先一一介紹左方說明欄，最左上的是畫筆，右上則是顏色的選擇，底下的range則是畫筆粗細，
再來兩個分別是控制shape的狀態，一個是實心空心，另一個是圖形是否為regular，
接下來便是4個shape，矩形、橢圓、三角形、還有線

底下則主要是一些功能
先是redo/undo/upload/download

再來是文字訊息跟橡皮擦，右下角的是clear

最下方的兩個element則是字體以及字大小的設定

而上述的修飾方式都記錄在css裡面就不再贅述，主要都是透過背景和網路下載的圖檔實現
而已上也大致就是index.html裡的內容
---------------------------------------------------------------
接下來就介紹是如何在javascript裡實現button跟input的功能


首先是先initial整個頁面以及畫布的狀況，再透過callback的方式讓load好再重新慢慢show出來。
接下來透過Canvas.toDataURL()這個finction還有一些Array跟index的方式，透過push()紀錄每次動作，
然後便可以用copy的屬性用ctx.drawImage(snap, 0, 0);來實現redo(),undo()

因為滑鼠位置算是最重要的變數，所以實作了mousePosition()來調整滑鼠位置跟畫布之間的關聯，得到正確位置。
接下來便是等到load好之後，便開始偵聽每個button的事件以及實現他，以下就大概提一些值得提的部分：

第一個實現的便是畫筆(pen)
因為是第一個button，所以也是花最多時間處理的，大多數的行數都在後面打了comment，主要是透過on()這個function，
來連結每個事件跟element的關係，但在這邊遇到的問題是，一開始用的是addEventListener，
但這樣要remove這個關係的時候就會很困難，因為在別的button實現時就找不到連結上去的function，
導致會重複寫入很多不同的function到'mousedown'這個event，所以我的方式是利用on()跟off()的function來實現這些關係，
註；但後來想想應該是連結'mousedown跟畫布之後再透過判斷button來決定要用哪個function。
所以pen的實作方式主要是透過'mousedown', 'mousemove', 'mouseup'，來處理每個橋段，後面的button也大多是如此。

第二個是透過一個bool的變數來記錄"實心空心"跟"正多邊形或任意"
並透過Jquery來實現一些動畫。

第三個是畫一些多邊形(rectangle, circle, triangle, line)
這邊一開始遇到的問題是會不斷的畫，只要不斷move就會畫，所以只能等到'mouseup'實計畫上去，
可是這樣再畫的時候就是一片空白，沒辦法知道實際到底畫多大了，而在實作redo/undo之後，
便可利用類似的方式，在每次畫完之後，在下次畫之前重新載入之前的圖片，便可以實現一般看到的功能了，
並且最後只在'mouseup'時push()，實現redo/updo。

第四個是(redo/undo)
主要是如同上面所說，透過Array儲存Data，透過index紀錄現在redo或undo的位置，利用ctx.drawImage(snap),0,0)來完整的畫出Data。

第五個是(upload/download)
這邊反而沒邏輯上的困難，比較難的是function較為複雜，所以需要清楚的知道每一步，但只要大致了解了，
這邊透過內建的function讀取大概就可以完成。

第六個是文字的部分(Text)
這邊也是弄最久的一部份，因為一開始一直不知道如何讓使用者自行輸入到Canvas，後來想透過append到Canvas也沒辦法，
後來利用了Textarea，並且是Append到Canvas的上一層element:div，等到使用者輸入完，並且點外面的時候，
便ctx.fillText()，也把剛剛append的textarea remove掉。這邊每一步都要注意，不管錯哪一步都很難de出bug，
所以也是我差點放棄的部分。

最後，第七個是(eraser/clear)
這邊大概就是利用clearRect()這個function，便可以實現清除的功能。

以上，而其他例如改變鼠標，或是一些input的讀取，甚至是顏色選擇等等，因為只是一些內建的function且也沒有邏輯上的問題，
通常只要打出來就不太會錯，就不贅述了。

